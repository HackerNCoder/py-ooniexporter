import requests, argparse, json
from datetime import datetime, timedelta
from prometheus_client import CollectorRegistry, Gauge, push_to_gateway

def fetchMeasurement(result):
    req = requests.get(result['measurement_url'], headers={'accept': 'application/json'})
    measurement = json.loads(req.text)
    return measurement

parser = argparse.ArgumentParser(prog = "web_connectivity", description="Python program to download ooni data on web_connectivity and export it for prometheus")
parser.add_argument("--country", help="Two letter country code to limit data to, default: none", metavar="US")
parser.add_argument("--since", help="Fetch data from given start date, default: yesterday", metavar="20200101")
parser.add_argument("--until", help="Fetch data until given end date, default: today", metavar="20200102")
parser.add_argument("--limit", help="The number of tests to fetch, default: 100", metavar="100")
args = parser.parse_args()

registry = CollectorRegistry()
website_per_cc = Gauge("ooni_tor_tests_per_country", "The total number successful and failing tests per country", labelnames=['probeCC', 'status'], registry=registry)

payload = {'test_name': 'tor'}
if args.country:
    payload['probe_cc'] = args.country

if args.since:
    payload['since'] = args.since
else:
    payload['since'] = (datetime.now() - timedelta(1)).strftime('%Y%m%d')

if args.until:
    payload['until'] = args.until
else:
    payload['until'] = datetime.now().strftime('%Y%m%d')

if args.limit:
    payload['limit'] = args.limit

print("fetching summary...")
r = requests.get('https://api.ooni.io/api/v1/measurements', params=payload, headers={'accept': 'application/json'})
print(r.url)
measurements = json.loads(r.text)

# resultdata = list()
i = 1
limit = len(measurements['results'])
for result in measurements['results']:
    if not result['failure']:
        print(f"reading result no {i} / {limit}")
        # measurement = fetchMeasurement(result)
        # resultdata.append(measurement)
        if result['anomaly']: #measurement['test_keys']['accessible']:
            if result['confirmed']:
                status = "confirmed"
            else:
                status = "anomaly"
        else:
            status = "OK"
        website_per_cc.labels(probeCC=result['probe_cc'], status=status).inc()
    i += 1
job=f"ooni_fetch_tor"
push_to_gateway('localhost:9091', job=job, registry=registry)
